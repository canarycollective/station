$(document).ready(function() { 
	
	$('.ajaxme').submit(function()
	{
        // // console.log('submitted form');
        var $identifier = $(this).attr('id');
		var $params = $(this).serialize();
        // // console.log($params);

		$.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $params,
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        // console.log('success! ');
                        $('.spinner').hide();
                        if(typeof response.uri!='undefined') window.location=response.uri;
                        else
                        {
                            var $displayme = '<div class="alert alert-success">'+response.happy_list+'</div>';
                            $('#'+$identifier+'-popup-hud').html($displayme);
                            scroll(0,0);
                        }
                    }
                    else
                    {
                        var $errors = response.error_list;
                        // // console.log($errors);
                        var $displayme = '<div class="alert alert-danger">'+$errors+'</div>';
                        //// // console.log($displayme);
                        $('#'+$identifier+'-popup-hud').html($displayme);
                        scroll(0,0);


                    }
                },
              
                timeout: function(response) {
                    // console.log(response);
                },
                error: function(one,two,three) {
                    // console.log('object:'+JSON.stringify(one));
                    // console.log('two: '+two);
                    // console.log('three: '+three);
                }
            });
            return false;
	});
});