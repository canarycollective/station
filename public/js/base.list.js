$(document).ready(function() { 
	
	/**
	 * a click on a list row will trigger the edit button for that row
	 *
	 */
	$('.station-list tr td').click(function(event) {
		
		window.location = $(this).closest('tr').find('a:first').attr('href');
		return false;
	});

	$('.station-list .station-filter').live('change',function()
	{
		alert('boosh!');
	});

	/**
	 * delete button clicked. launch modal and mark for deletion
	 *
	 */
	$('.list-record-deleter').click(function(event) {

		var id_to_delete = $(this).closest('tr').attr('id');
		$('.pending-deletion').removeClass('pending-deletion');
		$('#' + id_to_delete).addClass('pending-deletion');
		$('#deleter-modal').modal('show');
		event.stopPropagation();
		return false;
	});

	/**
	 * record deletion has been confirmed.
	 * send DELETE request to panel and id route
	 * 
	 */
	$('.deletion-confirmer').click(function(event) {
		
		$('#deleter-modal').modal('hide');
		$('.pending-deletion').find('td').css('background-color', '#ffffcc').closest('tr').fadeOut(900);

		$.ajax({

		    url: $('#deleter-modal').attr('data-relative-uri') + '/delete/' + $('.pending-deletion').attr('data-id'),
		    type: 'DELETE',
		    success: function(result) {
		        
		        // don't do anything. we will assume deleting was allowed and occured
		    }
		});

		return false;
	});
});	