<?php

	/**
	* This is a panel-level configuration
	*/

	return [
			'panel_options'	=> [
				'table'					=> 'stores',	//if no tablename, uses panel name
				'single_item_name'		=> 'Store',
				'where' 				=> 'user_id = "%user_id%"',
				'has_timestamps'		=> TRUE,	// Boolean for whether or not we'll be using timestamps
				'has_position'			=> FALSE,
				'default_order'			=> 'name',
				'no_data_force_create' 	=> TRUE,
				'no_data_alert' 		=> [

					'header' 	=> 'Let\'s Add Your Stores!',
					'body' 		=> 'If you have at least one store, please add it below. After saving, you can add more.'
				]
			],
			
			'elements'	=> [
				// These are the components that make up this panel
				'name'	=> [
					'label'			=> 'Store Name',
					'type'			=> 'text',
					'length'		=> 255,
					'attributes'	=> '',
					'rules'			=>	'required',
					'display'		=>	'CRUDL'
				],
				'industries'	=> [
					'label'			=> 'Primary Industries',
					'type'			=> 'multiselect',
					'multiple'		=> TRUE,
					'display'		=>	'CRUDL',
					'data'			=> [
						'join'		=> TRUE,
						'table'		=> 'industries',
						'pivot'		=> 'industries',
						'display'	=> 'industries.name',
						'order'		=> 'industries.name'
					]
				],
				'user_id'	=> [
					'label'			=> 'User ID',
					'type'			=> 'hidden',
					'default'		=> '%user_id%',
					'rules'			=> 'required',
					'display'		=> 'CRUDL'
				],
				'address'	=> [
					'label'			=> 'Physical Address',
					'help' 			=> 'We\'ll use our hi-tech mapping system to find it!',
					'type'			=> 'text',
					'length'		=> 255,
					'attributes'	=> '',
					'rules'			=>	'required',
					'display'		=>	'CRUDL'
				],
				'zip'	=> [
					'label'			=> 'Zip Code',
					'type'			=> 'text',
					'length'		=> 255,
					'attributes'	=> '',
					'rules'			=>	'required',
					'display'		=>	'CRUDL'
				]
			]
	];