<?php

	/**
	* This is a panel-level configuration
	*/

	return [
			'panel_options'	=> [
				'table'				=> 'users',	//if no tablename, uses panel name
				'single_item_name'	=> 'Account',
				'has_timestamps'	=> TRUE,	// Boolean for whether or not we'll be using timestamps
				'has_position'		=> FALSE,
				'default_order'		=> 'username'
			],
			
			'elements'	=> [
				// These are the components that make up this panel
				'username'	=> [
					'label'			=> 'Desired Username',
					'type'			=> 'text',
					'length'		=> 30,
					'attributes'	=> 'unique|index',
					'rules'			=>	'required|alpha_dash|unique:users,username|between:4,30', // this is wildcard for this table.
					'display'		=>	'CRUDL'
				],
				'password'	=> [
					'label'			=> 'Password',
					'type'			=> 'password',
					'help'		 	=> 'must be between 6 and 30 characters',
					'length'		=> 255, // important. must be greater than 60 characters
					'attributes'	=> '',
					'rules'			=>	'required|alpha_dash|between:6,30',
					'display'		=>	'CRUD'
				],
				'email'	=> [
					'label'			=> 'Email Address',
					'type'			=> 'email',
					'length'		=> 255,
					'attributes'	=> 'unique|index',
					'rules'			=>	'required|email|unique:users,email|max:90',
					'display'		=>	'CRUDL'
				],
				'first_name'	=> [
					'label'			=> 'First Name',
					'type'			=> 'text',
					'length'		=> 90,
					'attributes'	=> '',
					'rules'			=>	'required|max:90',
					'display'		=>	'CRUDL'
				],
				'last_name'	=> [
					'label'			=> 'Last Name',
					'type'			=> 'text',
					'length'		=> 90,
					'attributes'	=> '',
					'rules'			=>	'required|max:90',
					'display'		=>	'CRUDL'
				],
				'phone'	=> [
					'label'			=> 'Preferred Phone Number',
					'help' 			=> 'Optional',
					'type'			=> 'text',
					'length'		=> 90,
					'format'		=> 'phone',
					'attributes'	=> '',
					'rules'			=>	'max:90',
					'display'		=>	'CRUDL'
				],
				'initial_group_id'	=> [
					'label'			=> 'Account Type. Who Do You Represent?',
					'type'			=> 'radio',
					'rules'			=>	'required',
					'display'		=>	'CRUDL',
					'data'			=> [
						'options'		=> [
							5 => 'A Retailer',
							4 => 'A Vendor',
							3 => 'Both Retailer & Vendor'
						]
					]
				],
				'groups'	=> [
					'label'			=> 'Groups',
					'type'			=> 'multiselect',
					'multiple'		=> TRUE,
					'display'		=>	'',
					'data'			=> [				
						'join'		=> FALSE,
						'relation'	=> 'belongsToMany',
						'table'		=> 'groups',
						'pivot'		=> 'Canary\Station\Models\Group'

					]
				]
			]
	];