<?php

	/**
	* This is a panel-level configuration
	*/

	return [
			'panel_options'	=> [
				'table'				=> 'users',	//if no tablename, uses panel name
				'single_item_name'	=> 'Account',
				'has_timestamps'	=> TRUE,	// Boolean for whether or not we'll be using timestamps
				'has_position'		=> FALSE,
				'default_order'		=> 'username',
				'where' 			=> 'id = "%user_id%"'
			],
			
			'elements'	=> [
				// These are the components that make up this panel
				'username'	=> [
					'label'			=> 'Username',
					'type'			=> 'text',
					'disabled'		=> TRUE,
					'length'		=> 30,
					'attributes'	=> 'unique|index',
					'rules'			=>	'required|alpha_dash|unique:users,username|between:4,30', // this is wildcard for this table.
					'display'		=>	'CRUDL'
				],
				'email'	=> [
					'label'			=> 'Email Address',
					'type'			=> 'email',
					'length'		=> 255,
					'attributes'	=> 'unique|index',
					'rules'			=>	'required|email|unique:users,email|max:90',
					'display'		=>	'CRUDL'
				],
				'first_name'	=> [
					'label'			=> 'First Name',
					'type'			=> 'text',
					'length'		=> 90,
					'attributes'	=> '',
					'rules'			=>	'required|max:90',
					'display'		=>	'CRUDL'
				],
				'last_name'	=> [
					'label'			=> 'Last Name',
					'type'			=> 'text',
					'length'		=> 90,
					'attributes'	=> '',
					'rules'			=>	'required|max:90',
					'display'		=>	'CRUDL'
				],
				'phone'	=> [
					'label'			=> 'Preferred Phone Number',
					'help' 			=> 'Optional',
					'type'			=> 'text',
					'length'		=> 90,
					'format'		=> 'phone',
					'attributes'	=> '',
					'rules'			=>	'max:90',
					'display'		=>	'CRUDL'
				]
			]
	];