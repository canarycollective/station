<?php

	/**
	* This is the panel-level configuration for Posts : Station
	*/

	return [
			'panel_options'	=> [
				'table'				=> 'posts',	//if no tablename, uses panel name
				'single_item_name'	=> 'Post',
				'has_timestamps'	=> TRUE,	// Boolean for whether or not we'll be using timestamps
				'has_position'		=> FALSE,
				'default_order'		=> 'created_at'

			],
			
			'elements'	=> [
				// These are the components that make up this panel
				'title'	=> [
					'label'			=> 'Title',
					'type'			=> 'text',
					'length'		=> 90,
					'attributes'	=> '',
					'rules'			=>	'required|alpha_dash|unique:this|between:4,90', 
					'display'		=>	'CRUDL'
				],
				'body'	=> [
					'label'			=> 'Body',
					'type'			=> 'textarea',
					'length'		=> 70000, 
					'attributes'	=> '',
					'rules'			=>	'required|alpha_dash',
					'display'		=>	'CRUD'
				],
				'owner'	=> [
					'label'			=> 'Author',
					'type'			=> 'integer',
					'attributes'	=> '',
					'rules'			=>	'required',
					'display'		=>	'CRUDL',
					'data'			=> [				
						'join'		=> FALSE,
						'relation'	=> 'belongsTo', //possible vars are: 'hasOne','hasMany','belongsTo','belongsToMany' see: http://laravel.com/docs/eloquent#relationships
						'table'		=> 'users',
						'pivot'		=> 'user'
					]
				]

			]
			

	];