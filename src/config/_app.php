<?php 
	
	/**
	* This is the package-level configuration for Station
	*/

	return [

		'name'						=> 'LocalGear', 			// The name of your app or site 
		'root_admin_email' 			=> 'benhirsch@gmail.com',	// Email of the main administrator
		'root_uri_segment'			=> 'station', 				// The base url segment name. Ex. resolves to http://{host}/{root_uri_segment}/
		'panel_for_user_create' 	=> 'create_account',		// which panel do we use for creating a new user?
		'user_group_upon_register' 	=> 'input:initial_group_id', // when a new user registers, which group do we assign them?

		'user_groups' => [							

			'admin' => [

				'starting_panel' 	=> 'users.L',
				'description'		=> 'Has access to all the things.'
			],
			'manager' => [

				'starting_panel' 	=> 'users.L',
				'description'		=> 'Has access to most things.'
			],
			'hybrid' => [

				'starting_panel' 	=> 'company.C',
				'description'		=> 'Has access to vendor & retailer operations'
			],
			'vendor' => [

				'starting_panel' 	=> 'company.C',
				'description'		=> 'Has access to retailer operations.'
			],
			'retailer' => [

				'starting_panel' 	=> 'company.C',
				'description'		=> 'Has access to retailer operations.'
			],
			'standard' => [

				'starting_panel' 	=> 'welcome',
				'description'		=> 'Regular site user. Has limited access.'
			]
		],



		'panels' => [	// List of panels, in the order you want them to appear
						// Panel key is the file name for that panel's configuration. 
						// Ex. /app/config/packages/canary/station/{key}.php
			
			// pre-registration
			'create_account' => [
				'name'			=> 'Create An Account',
				'permissions'	=> [
					'anon'				=>	'C',
					'admin'				=>	'',
					'manager'			=>	'',
					'hybrid'			=> 	'',
					'vendor'			=> 	'',
					'retailer'			=> 	'',
					'standard'			=> 	''
				]
			],

			// post-registration
			'admin_sections' => [
				'is_seperator'	=> TRUE,
				'for_group'	=> [
					'anon'				=>	'',
					'admin'				=>	'Admin Sections',
					'manager'			=>	'Admin Sections',
					'hybrid'			=> 	'Profile',
					'vendor'			=> 	'Profile',
					'retailer'			=> 	'Profile',
					'standard'			=> 	'Profile'
				]
			],
				'my_account'		=> [
					'name'			=> 'My Account',
					'uri_slug'		=> 'update/%user_id%',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'U',
						'manager'			=>	'U',
						'hybrid'			=> 	'U',
						'vendor'			=> 	'U',
						'retailer'			=> 	'U',
						'standard'			=> 	'U'
					]
				],
				'users'		=> [
					'name'			=> 'Users',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'CRUDL',
						'manager'			=>	'CRUDL',
						'hybrid'			=> 	'',
						'vendor'			=> 	'',
						'retailer'			=> 	'',
						'standard'			=> 	''
					]
				],
				'groups'		=> [
					'name'			=> 'Groups',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'UL',
						'manager'			=>	'L',
						'hybrid'			=> 	'',
						'vendor'			=> 	'',
						'retailer'			=> 	'',
						'standard'			=> 	''
					]
				],
				'company'		=> [
					'name'			=> 'Company Profile',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'',
						'manager'			=>	'',
						'hybrid'			=> 	'CUL',
						'vendor'			=> 	'CUL',
						'retailer'			=> 	'CUL',
						'standard'			=> 	''
					]
				],

			'retailer_sections' => [
				'is_seperator'	=> TRUE,
				'for_group'	=> [
					'anon'				=>	'',
					'admin'				=>	'Retail',
					'manager'			=>	'Retail',
					'hybrid'			=> 	'Retail',
					'vendor'			=> 	'',
					'retailer'			=> 	'Retail',
					'standard'			=> 	''
				]
			],
				'our_stores'		=> [
					'name'			=> 'Stores',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'',
						'manager'			=>	'',
						'hybrid'			=> 	'CRUDL',
						'vendor'			=> 	'',
						'retailer'			=> 	'CRUDL',
						'standard'			=> 	''
					]
				],

			'journal' => [
				'is_seperator'	=> TRUE,
				'for_group'	=> [
					'anon'				=>	'',
					'admin'				=>	'Journals',
					'manager'			=>	'Journals',
					'hybrid'			=> 	'Company Journal',
					'vendor'			=> 	'Company Journal',
					'retailer'			=> 	'Company Journal',
					'standard'			=> 	''
				]
			],
				'posts'		=> [
					'name'			=> 'Journal Posts',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'CRUDL',
						'manager'			=>	'CRUDL',
						'hybrid'			=> 	'CRUDL',
						'vendor'			=> 	'CRUDL',
						'retailer'			=> 	'CRUDL',
						'standard'			=> 	''
					]
				],
				'comments'		=> [
					'name'			=> 'Comments',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'CRUDL',
						'manager'			=>	'CRUDL',
						'hybrid'			=> 	'CRUDL',
						'vendor'			=> 	'CRUDL',
						'retailer'			=> 	'CRUDL',
						'standard'			=> 	''
					]
				],

			'content_sections' => [
				'is_seperator'	=> TRUE,
				'for_group'	=> [
					'anon'				=>	'',
					'admin'				=>	'Content',
					'manager'			=>	'Content',
					'hybrid'			=> 	'',
					'vendor'			=> 	'',
					'retailer'			=> 	'',
					'standard'			=> 	''
				]
			],

				'industries'		=> [
					'name'			=> 'Industries',
					'permissions'	=> [
						'anon'				=>	'',
						'admin'				=>	'CRUDL',
						'manager'			=>	'CRUDL',
						'hybrid'			=> 	'',
						'vendor'			=> 	'',
						'retailer'			=> 	'',
						'standard'			=> 	''
					]
				]
		]
	];