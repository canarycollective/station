<?php

	/**
	* This is a panel-level configuration
	*/

	return [
			'panel_options'	=> [
				'table'					=> 'industries',	//if no tablename, uses panel name
				'single_item_name'		=> 'Industry',
				'where' 				=> '',
				'has_timestamps'		=> TRUE,	// Boolean for whether or not we'll be using timestamps
				'has_position'			=> FALSE,
				'default_order'			=> 'name',
				'no_data_force_create' 	=> FALSE
			],
			
			'elements'	=> [
				// These are the components that make up this panel
				'name'	=> [
					'label'			=> 'Name',
					'type'			=> 'text',
					'length'		=> 255,
					'attributes'	=> '',
					'rules'			=>	'required',
					'display'		=>	'CRUDL'
				]
			]
	];