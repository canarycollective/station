<?php namespace Canary\Station\Models;

use Config, Session, URL, DB, Input, Validator, Hash;
use Canary\Station\Models\Group as Group;

class Panel {

    /**
     * insert new record for this panel
     *
     * @param  string  $panel_name
     * @param  array  $data // for the new record
     * @return array or false // if user does not have access
     */
    public function create_record_for($panel_name, $user_scope, $is_for_user = FALSE){

        $writable_fields = $this->writable_fields($user_scope);
        $data            = Input::only($writable_fields); 
        $model_name      = $this->model_name_for($panel_name);
        $model           = new $model_name;

        if ($is_for_user) $data['password'] = Hash::make($data['password']);
        
        $model->fill($data);
        $model->save();
        $insert_id = $model->id;

        $this->attach_joins($model, $user_scope, $is_for_user);

        return $insert_id;
    }

    /**
     * return everything we can about this panel 
     * config, data, joins, etc.
     *
     * @param  string  $panel_name
     * @return array or false // if not found or user does not have access
     */
    public function get_data_for($panel_name, $count_only = FALSE){

        $panel              = $this->user_scope($panel_name, 'L');

        if (!$panel) return FALSE;

        $model_name         = $this->model_name_for($panel_name);
        $model              = new $model_name;
        $fields_for_select  = $this->fields_for_select($panel);
        $where_clause       = $this->where_clause_for($panel); 
        $joins              = $this->joins_for($panel);
        $query              = $model::select($fields_for_select);
        $query              = $where_clause ? $query->whereRaw($where_clause) : $query;
        $query              = count($joins) > 0 ? $query->with($joins) : $query;
        $panel['data']      = $count_only ? $query->count() : $query->take(999)->get()->toArray();

        return $panel;
    }


    /**
     * return everything we can about this panel but filtered
     *
     * @param  string  $panel_name
     * @param  array  $filter // where panel_name => constraining_id. essentially all the get data
     * @return array or false // if user does not have access
     */
    public function get_filtered_data_for($panel_name,$filter){


        $panel              = $this->user_scope($panel_name, 'L');
        $fields_for_select  = $this->fields_for_select($panel);
        $joins              = $this->joins_for($panel);

        $query = DB::table($panel_name);

        foreach($joins as $join)
        {
            $pivot = array();
            $single_from = str_singular($panel_name);
            $single_join = str_singular($join);
            $pivot[]   = $single_from;
            $pivot[]   = $single_join;
            $constraining_id = $filter[$join];

            sort($pivot);

            $pivot_table = $pivot[0] . '_' . $pivot[1];
            $query
                ->join($pivot_table,$panel_name.'.id','=',$pivot_table.'.'.$single_from.'_id')
                ->join($join,$join.'.id','=',$pivot_table.'.'.$single_join.'_id')
                ->where($pivot_table.'.'.$single_join.'_id','=',$constraining_id);
                        
            array_push($fields_for_select, $join.'.name as '.$join);

        }
        $data =         $query->select($fields_for_select)
                            ->get();

        //var_dump($data); exit;
        $panel['data'] = $data;

        return $panel;

    }

    /**
     * return everything we can about this panel but only the data for a 
     * single record in the corresponding table
     *
     * @param  string  $panel_name
     * @param  int  $id // of the record
     * @return array or false // if user does not have access
     */
    public function get_record_for($panel_name, $id){

        $panel              = $this->user_scope($panel_name, 'U');

        if (!$panel) return FALSE;

        $model_name         = $this->model_name_for($panel_name);
        $model              = new $model_name;
        $fields_for_select  = $this->fields_for_select($panel);
        $where_clause       = $this->where_clause_for($panel); 
        $joins              = $this->joins_for($panel);
        $query              = $model::select($fields_for_select);
        $query              = $where_clause ? $query->whereRaw($where_clause) : $query;
        $query              = count($joins) > 0 ? $query->with($joins) : $query;
        $panel['data']      = $query->find($id);

        return $panel;
    }

    /**
     * delete specific record for this panel
     *
     * @param  string  $panel_name
     * @param  int  $id // of the record
     * @return array or false // if user does not have access
     */
    public function delete_record_for($panel_name, $id){

        $model_name      = $this->model_name_for($panel_name);
        $model           = new $model_name;
        
        $model::destroy($id);

        return TRUE;
    }

    /**
     * update a single record for this panel
     *
     * @param  string  $panel_name
     * @param  array  $user_scope
     * @param  int  $id // of the record
     * @return array or false // if user does not have access
     */
    public function update_record_for($panel_name, $user_scope, $id){
        
        $writable_fields = $this->writable_fields($user_scope);
        $data            = Input::only($writable_fields); 
        $model_name      = $this->model_name_for($panel_name);
        $model           = new $model_name;
        
        $old_item = $model::find($id);
        $old_item->fill($data);
        $old_item->save();

        $this->attach_joins($old_item, $user_scope);

        return TRUE;
    }

    /**
     * accepts a panel name from config, for example 'users.L'
     * and returns the URI slug needed to redirect to.
     *
     * @param  string  $name
     * @return string // URI slug
     */
    static function config_to_uri($name){

        $base_uri       = Config::get('station::_app.root_uri_segment').'/';
        $name_arr       = explode('.', $name);
        $panel_name     = $name_arr[0];
        $letter         = isset($name_arr[1]) ? $name_arr[1] : 'L'; // list view by default?
        $panel_method   = static::method_name_for_letter($letter);

        return $base_uri.'panel/'.$panel_name.'/'.$panel_method; // TODO: <--- we need to add support for record numbers here
    }

    /**
     * for each element of a panel that a user can access
     * and which references a foreign table for data, get that data!
     *
     * @param  array  $user_scope
     * @return array // set of foreign tables' data, organized by element name as key
     */
    public function foreign_data_for($user_scope){

        $ret = [];

        foreach ($user_scope['config']['elements'] as $element_name => $element) {
            
            if (isset($element['data']['table']) && isset($element['data']['display'])){

                $table              = $element['data']['table'];
                $display            = $this->concat_clause($element['data']['display']);
                $where              = isset($element['data']['where']) ? $element['data']['where'] : FALSE;
                $order              = isset($element['data']['order']) ? $element['data']['order'] : FALSE;
                $query              = DB::table($table)->select('id', $display);
                $query              = $where ? $query->whereRaw($where) : $query;
                $query              = $order ? $query->orderBy($order) : $query;
                $data               = $query->get();
                $ret[$element_name] = $this->to_array($data);
            }
        }

        return $ret;
    }

    /**
     * convenience function to index an object by ID into an array 
     *
     * @param  mixed  $data // outer array with inner objects  
     * @return array
     */
    private function to_array($data) {

        $ret = [];

        foreach ($data as $item) {
            
            $ret[$item->id] = $item->value;
        }

        return $ret;
    }

    /**
     * convert terms into SELECT SQL clause  
     *
     * @param  mixed  $terms 
     * @return string // SQL clause
     */
    private function concat_clause($terms){

        if (is_array($terms)){

            $arr = [];

            foreach ($terms as $term) {
                
                $arr[] = strlen($term) < 2 ? "'".$term."'" : $term;
            }

            return 'CONCAT('.implode(',', $arr).') AS value';

        } else {

            return $terms.' AS value';
        }
    }

    /**
     * converts a single letter from a CRUDL letter string 
     * returns the URI segment name
     *
     * @param  string  $letter
     * @return string // URI segment name
     */
    static function method_name_for_letter($letter = 'L'){

        switch (substr($letter, 0, 1)) {

            case 'L':
                
                return 'index';
                break;

            case 'C':
                
                return 'create';
                break;

            case 'R':
                
                return 'show';
                break;

            case 'U':
                
                return 'edit';
                break;

            case 'D':
                
                return 'delete';
                break;
            
            default:
                
                return 'index';
                break;
        }
    }

    /**
     * returns the determined model name for the specified panel
     *
     * @param  array  $panel // full configuration from station::{panel-name} config file  
     * @return string // the model name
     */
    public function model_name_for($panel_name){

        switch ($panel_name) {

            case 'users':
                return 'Canary\Station\Models\User';
                break;

            case 'groups':
                return 'Canary\Station\Models\Group';
                break;
            
            default:
                return ucwords(str_singular($panel_name));
                break;
        }
    }

    /**
     * return panel config as seen from user perspective.
     * elements are filtered down to only the ones visible to this user.
     *
     * @param  string  $panel_name
     * @param  string  $letter // of CRUDL
     * @return array or false // if user does not have access
     */
    public function user_scope($panel_name, $letter = 'L'){

        $ret              = array();
        $panels           = Config::get('station::_app.panels');
        $primary_role     = Session::get('user_data.primary_group');

        if (!$primary_role) $primary_role = 'anon';

        $has_panel_access = isset($panels[$panel_name]['permissions'][$primary_role]) 
                            && strpos($panels[$panel_name]['permissions'][$primary_role], $letter) !== FALSE;

        if ($has_panel_access){ // can access panel AND can perform $letter method

            $ret['config'] = Config::get('station::'.$panel_name); // start with the whole config!
            $ret['config']['elements'] = $this->elements_for_letter($ret['config'], $letter); // but filter the elements.
            $ret['config']['elements'] = $this->inject_vars_for_elements($ret['config']['elements']);

            if (count($ret['config']['elements']) == 0) return FALSE; // no elements, no access for you!

            return $ret;
        }

        return FALSE;
    }
    
    /**
     * returns array of panels that a user can access 
     *
     * @return array
     */
    public function user_panel_access_list(){

        $ret            = array();
        $base_uri       = Config::get('station::_app.root_uri_segment');
        
        $primary_role   = Session::get('user_data.primary_group');
        $panels         = Config::get('station::_app.panels');


        foreach($panels as $panel_name => $panel_data)
        {
            if((isset($panel_data['permissions'][$primary_role]) 
                && $panel_data['permissions'][$primary_role]!='none' 
                && $panel_data['permissions'][$primary_role] != '')
                || (isset($panel_data['for_group'])
                && isset($panel_data['is_seperator'])))
            {
                $slug = isset($panel_data['uri_slug']) ? $this->inject_vars($panel_data['uri_slug']) : 'index';
                $uri = $base_uri.'/panel/'.$panel_name.'/'.$slug;
                $seperator_name = isset($panel_data['for_group'][$primary_role]) ? $panel_data['for_group'][$primary_role] : '';
                $ret[] = [

                    'panel' => $panel_name,
                    'uri'   => URL::to($uri),
                    'label' => isset($panel_data['name']) ? $panel_data['name'] : $seperator_name,
                    'is_seperator' => isset($panel_data['is_seperator']) && $panel_data['is_seperator']
                ];
            }
        }

        return $ret;
    }

    /**
     * checks all inputs from user against validation object and rules from the panel config
     *
     * @param  string  $panel_name
     * @param  array  $user_scope
     * @return object Validator
     */
    public function validates_against($panel_name, $user_scope, $unique_id = FALSE){

        $inputs = [];
        $rules  = [];

        foreach ($user_scope['config']['elements'] as $element_name => $element) {
            
            if (isset($element['rules'])) {
                
                $inputs[$element_name] = Input::get($element_name);
                $rules[$element_name]  = $this->filter_element_rules($element['rules'], $unique_id);
            }
        }

        return Validator::make($inputs, $rules);
    }

    /**
     * right now this just adds an 'ignore' ID to any 'unique' validation rule when we are updating that record
     * this could easily accept more filters if needed.
     *
     * @param  string  $rules
     * @param  int  $unique_id
     * @return string // filtered rules set
     */
    private function filter_element_rules($rules, $unique_id = FALSE){

        if (!$unique_id || $rules == '') return $rules; // no unique ID, we don't care.

        $rules_arr = explode('|', $rules);
        $new_rules = [];

        foreach ($rules_arr as $rule) {
            
            if (strpos($rule, 'unique:') !== FALSE){

                $new_rules[] = $rule.', '.$unique_id;
            
            } else {

                $new_rules[] = $rule;
            }
        }

        return implode('|', $new_rules);
    }

    private function attach_joins($model, $user_scope, $for_user = FALSE){

        if ($for_user) {
            
            $starting_group_name = Config::get('station::_app.user_group_upon_register');

            if (strpos($starting_group_name, 'input:') !== FALSE){

                // group is set by choice made at registration
                $str_arr          = explode(':', $starting_group_name);
                $value_from_input = Input::get($str_arr[1]);
                $lowest_group_id  = Group::where('name', '=', 'standard')->pluck('id'); // TODO: change. may not always have a standard group!
                $group_id         = is_numeric($value_from_input) ? $value_from_input : $lowest_group_id;

            } else { // new user is forced into a group

                $group_id = Group::where('name', '=', $starting_group_name)->pluck('id');
            }
            
            $model->groups()->attach($group_id);
        
        } else {

            $joins = $this->joins_for($user_scope);

            foreach ($joins as $join) {
                
                $model->$join()->sync(Input::get($join));
            }
        }
    }

    /**
     * returns array of elements which can be accessed given a CRUDL letter / method
     *
     * @param  array  $panel_config // full config of panel
     * @param  string  $letter // method letter of CRUDL
     * @return array // elements, filtered
     */
    private function elements_for_letter($panel_config, $letter = 'L'){

        $ret = [];

        foreach ($panel_config['elements'] as $element_name => $element) {
            
            $hidden_in_list = $letter == 'L' && $element['type'] == 'hidden';

            if (strpos($element['display'], $letter) !== FALSE && !$hidden_in_list){

                $ret[$element_name] = $element;
            }
        }

        return $ret;
    }

    /**
     * return array of fields that can be selected directly from table (not joins)
     *
     * @param  array  $panel // the full panel config
     * @return array // field names
     */
    private function fields_for_select($panel){

        $table_name = $panel['config']['panel_options']['table'];
        $ret = [$table_name.'.id'];

        foreach ($panel['config']['elements'] as $element_name => $element) {
            
            if (!isset($element['data']['join'])){

                $ret[] = $table_name.'.'.$element_name;
            }
        }

        return $ret;
    }

    private function inject_vars($str){

        $replacements  = ['%user_id%' => Session::get('user_data.id')];

        return str_replace(array_keys($replacements), array_values($replacements), $str);
    }

    private function inject_vars_for_elements($elements){

        if (Session::get('user_data')){

            $ret               = [];
            $fields_to_replace = ['default'];

            foreach ($elements as $name => $element) {
                
                $ret[$name] = $element;

                foreach ($fields_to_replace as $field) {
                    
                    if (isset($ret[$name][$field])){

                        $ret[$name][$field] = $this->inject_vars($ret[$name][$field]);
                    }
                }
            }

            return $ret;

        } else {

            return $elements;
        }
    }

    /**
     * returns a list of join tables for the specified panel
     *
     * @param  array  $panel // full configuration from station::{panel-name} config file  
     * @return array // join tables
     */
    private function joins_for($panel){

        $ret = [];

        foreach ($panel['config']['elements'] as $element_name => $element) {
            
            if (isset($element['data']['pivot']) || isset($element['data']['table'])){

                $ret[] = isset($element['data']['pivot']) ? $element['data']['pivot'] : $element['data']['table'];
            }
        }

        return $ret;
    }

    private function where_clause_for($panel){

        if (isset($panel['config']['panel_options']['where']) && $panel['config']['panel_options']['where'] != '') {
            
            if (Session::get('user_data')){

                return $this->inject_vars($panel['config']['panel_options']['where']);

            } else {

                return $panel['config']['panel_options']['where'];
            }
        }

        return FALSE;
    }

    /**
     * given the fields we know a user has access to for method they are in 
     * return a list of element/field names that can be written to DB
     *
     * @param  array  $user_scope
     * @return array // element names
     */
    private function writable_fields($user_scope){

        $ret = [];
        $non_writable_field_types = ['multiselect', 'virtual'];

        foreach ($user_scope['config']['elements'] as $element_name => $element) {
            
            if (!in_array($element['type'], $non_writable_field_types)){

                $ret[] = $element_name;
            }
        }

        return $ret;
    }

    public function add_all_to_array($passed_array)
    {
        foreach($passed_array as &$array)
        {
            $array = [0=>'All'] + $array;
        }
        return $passed_array;
    }
}