<?php namespace Canary\Station\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem as File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;
use Schema;
use Canary\Station\Models\Panel as Panel;

class Build extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'station:build';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Rebuilds the station package (generates migrations, migrates, and seeds)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->pivots = array();
		$this->valid_relationships = array('hasOne','hasMany','belongsTo','belongsToMany');
		//$this->file = $file;
	}

	/**
	 * Execute the console command.
	 * This builds the station up to latest DB, Seeds, and Panel Config.
	 * 
	 * runs generate:migration for each panel 
	 * runs generate:pivot for pivot tables 
	 * runs migrate 
	 * runs db:seed using the StationDatabaseSeeder
	 *
	 * @return void
	 */
	public function fire()
	{
		//return View::make('crunches.create');
        //$users = Config::get('station::users');

        $config = Config::get('station::_app');

        $this->generate_migrations($config);
        $this->generate_models($config);
        $this->call('db:seed',array('--class'=>"StationDatabaseSeeder")); //test
	}

	private function generate_migrations($config){

		foreach($config['panels'] as $filename => $fileinfo)
        {

			$data			= Config::get('station::'.$filename);
			$table_name		= $data['panel_options']['table'];

			if (!isset($data['panel_options']['table'])) continue;

			$fields_string	= $this->fields_string_for_elements($table_name, $data);

        	if (Schema::hasTable($table_name)){

        		foreach($data['elements'] as $att_name => $att_data){
        			
        			if (!Schema::hasColumn($table_name, $att_name) && !isset($att_data['data']['join'])) { // we don't have the column and it's not a join.

        				// ... add migration for it.
        				$migration_name = 'add_'.$att_name.'_column_to_'.$table_name.'_table'; // must keep 'column_to'!
        				$migration_options = array('name' => $migration_name,'--fields' => $att_name.$this->column_type_suffix($att_data['type']));
        				$this->generate_migration($migration_options);
        			}
        		}

        	} else if ($fields_string != '') { // we don't have this table, but we have fields so, let's create it

        		$migration_options = array('name' => 'create_'.$table_name.'_table','--fields' => $fields_string);
        		$this->generate_migration($migration_options);
        	}

        	$this->call('migrate');
        }

        $this->generate_pivots();

        if (!Schema::hasTable('password_reminders')) {
        	
        	$this->call('auth:reminders'); // this is needed for using auth. 
        }

        return TRUE;
	}

	/**
	 * fires the generate:migrations command
	 *
	 * @param  array  $options
	 * @return void
	 */
	private function generate_migration($options){

		$this->call('generate:migration', $options);
	}

	/**
	 * fire generate:pivot for any pivot tables that we need which do not already exist.
	 *
	 * @return void
	 */
	private function generate_pivots(){

        if(count($this->pivot)>0)
        {
        	foreach($this->pivot as $pivot)
        	{
        		$pivot_table_name = $this->pivot_table_name($pivot);

        		if (!Schema::hasTable($pivot_table_name)) {

        			$this->call('generate:pivot',$pivot);
        		}
        		$this->call('migrate');
        	}
        }
	}

	/**
	 * the pivot table name.
	 * makes compound table name from singular versions of foreign table names
	 *
	 * @param  array  $pivot // consists of the foreign table names
	 * @return string
	 */
	private function pivot_table_name($pivot, $with_keys = FALSE){

        $tableOne 	= str_singular($pivot['tableOne']);
        $tableTwo 	= str_singular($pivot['tableTwo']);
        $tables 	= array($tableOne, $tableTwo);

        sort($tables);

        if ($with_keys) {
        	
        	return ", '".$tables[0]."_".$tables[1]."', '".str_singular($tableOne)."_id', '".str_singular($tableTwo)."_id'";

        } else {

        	return $tables[0].'_'.$tables[1];
        }
	}

	/**
	 * creates string for generator 
	 *
	 * @param  string  $table_name
	 * @param  string  $data // panel data
	 * @return string
	 */
	private function fields_string_for_elements($table_name, $data){

		$fields		= '';

        foreach($data['elements'] as $att_name => $att_data)
        {
        	$fields .= $this->process_element($table_name,$att_name,$att_data);
        }

        $fields = rtrim($fields,', ');

        return $fields;
	}

	/**
	 * converts panel element to a string which can be passed to way's generate:migration
	 *
	 * @param  string  $table_name  // table name
	 * @param  string  $name // element name
	 * @param  array  $data // element config data
	 * @return string // empty if we need a pivot on this element
	 */
	public function process_element($table_name,$name,$data)
	{
		$ret = $name;

		// add a pivot if join is required
		// TODO: extract this
		if(isset($data['data']['join']) && $data['data']['join'])
		{
			$this->pivot[] = array('tableOne'=>$table_name,'tableTwo'=>$data['data']['pivot']);
			return '';
		}
		elseif(isset($data['data']['join']) && !$data['data']['join']) return '';
		
		$ret .= $this->column_type_suffix($data['type']);

		if(isset($data['length'])) $ret .= '['.$data['length'].']';

		if(isset($data['attributes']) && $data['attributes'] != '')
		{
			$attribs = explode('|', $data['attributes']);

			foreach($attribs as $attribute)
			{
				if($attribute!='')	$ret .= ':'.$attribute;
			}
		}

		return $ret.', ';
	}

	/**
	 * suffix/extension for generator fields
	 *
	 * @param  string  $type
	 * @return string
	 */
	private function column_type_suffix($type = 'text'){

		switch($type)
		{
			case 'text':
				$ret = ':string';
				break;
			case 'email':
				$ret = ':string';
				break;
			case 'password':
				$ret = ':string';
				break;
			case 'date':
				$ret = ':date';
				break;
			case 'radio':
				$ret = ':integer';
				break;
			case 'select':
				$ret = ':integer';
				break;
			case 'textarea':
				$ret = ':text';
				break;
			default:
				$ret = ':string';
				break;
		}

		return $ret;
	}

	/**
	 * generates and saves any models that are needed. It ignores User and Group panels.
	 *
	 * @param  none
	 * @return void
	 */
	private function generate_models($config){

		$this->file = new File;
		$this->panel_model = new Panel;

		foreach($config['panels'] as $panel_name => $panel_general)
		{
			if(in_array($panel_name, array('users','groups'))) continue; // Skipping over these

			$panel_data	= Config::get('station::'.$panel_name);
			$className	= $panel_data['panel_options']['single_item_name'];
			$modelName	= $this->panel_model->model_name_for($panel_name);
			$tableName	= $panel_data['panel_options']['table'];
			$filePath	= app_path() . '/models/'.$modelName.'.php';

			if($this->file->exists($filePath))	// we are just going to rebuild the section that is generated
			{
				$new_model = "//GEN-BEGIN\n\n\tprotected \$table = '$tableName';\n\tprotected \$guarded = array('id');\n";

				if(!$panel_data['panel_options']['has_timestamps']) $new_model .= "\tpublic \$timestamps = false;\n\n";
				else $new_model .= "\n";

				foreach($panel_data['elements'] as $elem_name => $elem_data)
				{
					if(isset($elem_data['data']['relation']) && in_array($elem_data['data']['relation'], $this->valid_relationships))
					{
						$pivot_table = strpos($elem_data['data']['relation'], 'Many') !== FALSE ? $this->pivot_table_name(array('tableOne' => $tableName, 'tableTwo' => $elem_data['data']['table']), TRUE) : '';
						$new_model .= "\tpublic function ".$elem_data['data']['table']."()\n\t{\n\t\treturn \$this->".$elem_data['data']['relation']."('".ucfirst($elem_data['data']['pivot'])."'".$pivot_table.");\n\t}\n";
					}
				}

				$new_model .= "\t//GEN-END";

				$existing_model = $this->file->get($filePath);

				$start = explode('//GEN-BEGIN',$existing_model);
   				$end = explode('//GEN-END',$start[1]);

   				$new_model = $start[0] . $new_model . $end[1];
			}
			else 	// totally new model to write!
			{
				$new_model = "<?php\n\nclass $modelName extends Eloquent {\n\t//GEN-BEGIN\n\n\tprotected \$table = '$tableName';\n\tprotected \$guarded = array('id');\n\n";

				if(!$panel_data['panel_options']['has_timestamps']) $new_model .= "\tpublic \$timestamps = false;\n\n";
				else $new_model .= "\n";

				if (!isset($panel_data['elements'])) continue;
				
				foreach($panel_data['elements'] as $elem_name => $elem_data)
				{
					if(isset($elem_data['data']['relation']) && in_array($elem_data['data']['relation'], $this->valid_relationships))
					{
						$pivot_table = strpos($elem_data['data']['relation'], 'Many') !== FALSE ? $this->pivot_table_name(array('tableOne' => $tableName, 'tableTwo' => $elem_data['data']['table']), TRUE) : '';
						$new_model .= "\tpublic function ".$elem_data['data']['table']."()\n\t{\n\t\treturn \$this->".$elem_data['data']['relation']."('".ucfirst($elem_data['data']['pivot'])."'".$pivot_table.");\n\t}\n";
					}
				}

				$new_model .= "\t//GEN-END\n\n\t// Feel free to add any new code after this line\n}";
			}

			$this->file->put($filePath,$new_model);
		}
	}
}