<?php namespace Canary\Station\Controllers;

use Password, Input, Config, Session, Redirect, View, Hash, Auth;

class StationUserController extends ObjectBaseController {

	public function __construct()
    {
        parent::__construct();
        $this->base_uri = Config::get('station::_app.root_uri_segment').'/';
        $this->app = Config::get('station::_app');
        $this->data['is_logged_in'] = Auth::check();
    }

    public function do_reset_password(){

        $credentials = array(

            'email'                 => Input::get('email'),
            'password'              => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation')
        );

        return Password::reset($credentials, function($user, $password)
        {
            $user->password = Hash::make($password);
            $user->save();
            Auth::login($user);
            return Redirect::to($this->base_uri.'home');
        });
    }

    /**
     * handles sending the user a message with password reset info
     *
     * @return void
     */
    public function forgot(){

    	$credentials = array('email' => Input::get('email'));

    	return Password::remind($credentials, function($message, $user)
        {
            $message->subject('Your Password Reminder');
        });
    }

    /**
     * display the form for password reset 
     *
     * @param  string  $token
     * @return View
     */
    public function password_reset($token = ''){

        return View::make('station::user.password_reset')->with('token', $token);
    }

    /**
     * this is the landing page for the forgotten password / reset password form 
     * it just redirects to the login form.
     */
    public function reminded(){

        if (Session::has('success')){

            $status  = 'success';
            $message = 'We sent a password reset link to your email. Please take a look.';

        } else {

            $status  = 'error';
            $message = 'We could not find that email address in our system';
        }

        return Redirect::to($this->base_uri.'login')->with($status, $message);
    }
}