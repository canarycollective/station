<?php namespace Canary\Station\Controllers;

use View, App, Config, Session, Auth, Redirect, URL, Input, Response;
use Canary\Station\Models\Panel as Panel;
use Canary\Station\Filters\Session as Station_Session;
use Illuminate\Filesystem\Filesystem as File;

class StationPanelController extends \BaseController {

	protected $layout = 'station::layouts.base';

    protected function init($panel_name, $method = 'L', $id = 0){

    	$panel 					= new Panel;
		$this->id				= $id;
		$this->name				= $panel_name;
		$this->app_config		= Config::get('station::_app');
		$this->user_scope		= $panel->user_scope($this->name, $method);

		// temp handling if someone is trying to access something theyshouldn't		
		if(!$this->user_scope) dd('You do not have access to this');
		
		$this->panel_config 	= $this->user_scope['config'];
		$this->single_item_name	= $this->panel_config['panel_options']['single_item_name'];
		$this->user_data		= Session::get('user_data');
		$this->assets			= isset($this->assets) ? $this->assets : [];
		$this->base_uri			= Config::get('station::_app.root_uri_segment').'/';
		$this->foreign_data 	= $panel->foreign_data_for($this->user_scope, $method);

		$this->panel_config['relative_uri']	= $this->base_uri.'panel/'.$this->name;
		$this->panel_config['relative_uri']	= URL::to($this->panel_config['relative_uri']);

		View::share('curr_panel', $this->name);
		View::share('single_item_name', $this->single_item_name);
        View::share('app_data', $this->app_config);
        View::share('panel_data', $this->panel_config);
        View::share('panel_name', $this->name);
        View::share('foreign_data', $this->foreign_data);
        View::share('user_data', $this->user_data);
        View::share('sidenav_data', $panel->user_panel_access_list());
    }

	public function index($panel_name){

		$panel		= new Panel;

		if (count(Input::all()) > 0) $panel_data = $panel->get_filtered_data_for($panel_name, Input::all()); 
		else $panel_data = $panel->get_data_for($panel_name);

		if (!$panel_data) return Redirect::back()->with('error', 'You do not have access to that area.');

		$this->init($panel_name, 'L');

		$no_data_and_force_create = count($panel_data['data']) == 0 && isset($this->panel_config['panel_options']['no_data_force_create']);
		if ($no_data_and_force_create) return Redirect::to($this->panel_config['relative_uri'].'/create');

		//dd($panel_data);

		$this->foreign_data = $panel->add_all_to_array($this->foreign_data);
		$user_can_create = $this->can_create($panel_name);
		$user_can_update = $this->can_update($panel_name);
		$user_can_delete = $this->can_delete($panel_name);
		$title	= str_plural($this->single_item_name);

		View::share('foreign_data', $this->foreign_data);
		View::share('user_can_create', $user_can_create);
		View::share('user_can_update', $user_can_update);
		View::share('user_can_delete', $user_can_delete);
		View::share('page_title', $title);
		View::share('layout_title', $title);
		View::share('data', $panel_data);
		
		return $this->render('list');
	}

	public function create($panel_name){

		$this->init($panel_name, 'C');

		$user_can_create = $this->can_create($panel_name);

		if (!$user_can_create) return Redirect::back()->with('error', 'You do not have access to that area.');
		
		$panel = new Panel;
		$panel_data_with_count = $panel->get_data_for($panel_name, TRUE);

		$title = 'Create a new '.$this->single_item_name;
		View::share('n_items_in_panel', $panel_data_with_count['data']);
		View::share('page_title', $title);
		View::share('layout_title', $title);
		View::share('form_action', $this->base_uri.'panel/'.$this->name);
		View::share('form_method', 'POST');
		View::share('form_purpose','create');
		View::share('submit_value', 'Save this '.$this->single_item_name);
		return $this->render('form');
	}

	public function create_user(){

		if (Auth::check()) return Redirect::to(Config::get('station::_app.root_uri_segment').'/login');

		$this->assets['css'][] = 'login.css';
		$panel_name = Config::get('station::_app.panel_for_user_create');
		return $this->create($panel_name);
	}

	public function do_create($panel_name){

		$panel			= new Panel;
		$user_scope 	= $panel->user_scope($panel_name, 'C'); // can we create? which fields?

		if (!$user_scope) return Redirect::back()->with('error', 'You do not have access to that area.');

		$this->init($panel_name, 'C');

		$validator 	 = $panel->validates_against($panel_name, $user_scope);
		$attempt_uri = isset($this->is_creating_user) ? '/'.$this->base_uri.'register/' : '/'.$this->base_uri.'panel/'.$panel_name.'/create';

		if ($validator->fails()){ // flash error and redirect to form

			return Redirect::to($attempt_uri)->withErrors($validator)->withInput();
		
		} else { // save and redirect

			$record_id = $panel->create_record_for($panel_name, $user_scope, isset($this->is_creating_user));

			if (is_numeric($record_id)){

				if (isset($this->is_creating_user)){ // this is a new account. find out where they should go first, log them in and redirect

					Auth::loginUsingId($record_id);
					Station_Session::hydrate();

					$uri			= Session::get('user_data.starting_panel_uri');
					$message		= 'Your account has been added and you are logged in!';

				} else { // just creating a new record in general

					$uri		= '/'.$this->base_uri.'panel/'.$panel_name;
					$message	= 'Your new '.strtolower($this->single_item_name).' has been added';
				}

				return Redirect::to($uri)->with('success', $message);
			}

			return Redirect::to($attempt_uri)->with('error', 'Something went wrong with saving :('); // TODO: log this.
		}
	}

	public function do_create_user(){

		if (Auth::check()) return Redirect::to(Config::get('station::_app.root_uri_segment').'/login');

		$panel_for_user_create = Config::get('station::_app.panel_for_user_create');
		$this->is_creating_user = TRUE;
		return $this->do_create($panel_for_user_create);
	}

	public function update($panel_name, $id){

		$panel		= new Panel;
		$panel_data	= $panel->get_record_for($panel_name, $id);

		if (!$panel_data || !$panel_data['data']) return Redirect::back()->with('error', 'You do not have access to that area.');

		$this->init($panel_name, 'U', $id);
		$title = 'Edit This '.$this->single_item_name;

		View::share('passed_model', $panel_data['data']);
		View::share('page_title', $title);
		View::share('layout_title', $title);
		View::share('form_action', $this->base_uri.'panel/'.$this->name.'/update/'.$id);
		View::share('form_method', 'PUT');
		View::share('form_purpose', 'update');
		View::share('submit_value', 'Save this '.$this->single_item_name);
		return $this->render('form');
	}

	public function do_update($panel_name, $id){

		$panel			= new Panel;
		$user_scope 	= $panel->user_scope($panel_name, 'U'); // can we update? which fields?

		if (!$user_scope) return Redirect::back()->with('error', 'You do not have access to that area.');

		$this->init($panel_name, 'U', $id);

		$validator 	 = $panel->validates_against($panel_name, $user_scope, $id);
		$attempt_uri = '/'.$this->base_uri.'panel/'.$panel_name.'/update/'.$id;

		if ($validator->fails()){ // flash error and redirect to form

			return Redirect::to($attempt_uri)->withErrors($validator)->withInput();
		
		} else { // save and redirect

			if ($panel->update_record_for($panel_name, $user_scope, $id)){

				$uri		= '/'.$this->base_uri.'panel/'.$panel_name;
				$edit_uri 	= $uri.'/update/'.$id;
				$message	= 'This '.strtolower($this->single_item_name).' has been edited. Make <a href="'.$edit_uri.'">more edits</a>.';

				return Redirect::to($uri)->with('success', $message);
			}

			return Redirect::to($attempt_uri)->with('error', 'Something went wrong with saving :(');
		}
	}

	public function do_delete($panel_name, $id){

		$this->init($panel_name, 'D', $id);
		$user_can_delete = $this->can_delete($panel_name);

		if ($user_can_delete) {
			
			$panel = new Panel;
			$panel->delete_record_for($panel_name, $id);
			return Response::json(array('status' => '1', 'message' => 'The record was deleted'));
		}

		return Response::json(array('status' => '0', 'message' => 'The record was not deleted'));
	}

	private function can_create($panel_name){

		if ((isset($this->user_data['primary_group']) 
			&& strpos($this->app_config['panels'][$panel_name]['permissions'][$this->user_data['primary_group']],'C') !== FALSE)
			|| $panel_name == $this->app_config['panel_for_user_create']){

			return TRUE;
		}

		return FALSE;
	}

	private function can_update($panel_name){

		if ((isset($this->user_data['primary_group']) 
			&& strpos($this->app_config['panels'][$panel_name]['permissions'][$this->user_data['primary_group']],'U') !== FALSE)){

			return TRUE;
		}

		return FALSE;
	}

	private function can_delete($panel_name){

		if ((isset($this->user_data['primary_group']) 
			&& strpos($this->app_config['panels'][$panel_name]['permissions'][$this->user_data['primary_group']],'D') !== FALSE)){

			return TRUE;
		}

		return FALSE;
	}

	private function render($template = 'list', $model = FALSE){

		switch ($template) {

			case 'form':
				
				$this->assets['js'][] = 'base.form.js';
				$this->assets['js'][] = 'chosen_v1.0.0/chosen.jquery.min.js';
				$this->assets['css'][] = 'chosen.css';
				break;

			case 'list':
				
				$this->assets['js'][] = 'base.list.js';
				break;
		}

		View::share('panel_name', $this->name);
		View::share('assets', $this->assets);

		$this->layout->content = View::make('station::layouts.'.$template);		
	}
}