<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ Config::get('station::_app.name') }} | Please Reset Your Password</title>

    <!-- Bootstrap core CSS -->
    <link href="/packages/canary/station/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/packages/canary/station/css/login.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>

    <div class="container">

      {{ Form::open(array('url' => Config::get('station::_app.root_uri_segment').'/password/reset/'.$token, 'class' => 'form-signin reset-password')) }}

        <h2 class="form-signin-heading">Please reset your password</h2>

        @if(Session::has('error'))
            <div class="alert alert-danger">
              {{ trans(Session::get('reason')) }}
            </div>
        @endif

        <input class="form-control" type="hidden" name="token" value="{{ $token }}">
        <input class="form-control" type="text" name="email" placeholder="Email Address" required autofocus>
        <input class="form-control" type="password" name="password" placeholder="New Password" required>
        <input class="form-control" type="password" name="password_confirmation" placeholder="New Password Again" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Reset My Password</button>

      {{ Form::close() }}

    </div> <!-- /container -->
  </body>
</html>
