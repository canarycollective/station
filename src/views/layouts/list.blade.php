@extends('station::layouts.base')

@section('content')

	@if (count($data['data']) > 0)

		<div class="clearfix">
			<div class="pull-left">
				<h5>{{ $layout_title }} &nbsp; <small>{{ count($data['data']) > 1 ? count($data['data']) : '' }}</small></h5>
			</div>
			<div class="pull-right">
				{{-- We need a create new button! --}}
				@if ($user_can_create)
					<a href="{{ $panel_data['relative_uri'].'/create' }}" class="btn btn-lg btn-success list-new">
						<span class="fui-plus"></span>&nbsp 
						Create a New {{ $single_item_name }}
					</a>
				@endif
			</div>
		</div>

		{{-- First we build the table header--}}
		<table class="table table-hover station-list">
			<thead>
				<tr>
					@if ($user_can_update)
						<th>{{-- For our update/edit buttons column --}}</th>
					@endif

					@foreach($data['config']['elements'] as $elem_name => $elem_data)
						<th>
							@if (isset($foreign_data[$elem_name]) && count($foreign_data[$elem_name]) > 0)
								{{ Form::select('filter-'.$elem_name, $foreign_data[$elem_name], null,['class'=>'table-filter']) }}
							@else 
								{{ $elem_data['label'] }}
							@endif
						</th>
					@endforeach

					@if ($user_can_delete)
						<th>{{-- For our delete buttons column --}}</th>
					@endif
				</tr>
			</thead>
			<tbody>
				@foreach($data['data'] as $row)
					<?php $row = (array) $row; ?>

					<tr id="record-{{  $row['id'] }}" data-id="{{  $row['id'] }}">
						@if ($user_can_update)
							{{-- We need our button for edit/update --}}
							<td class="td-for-edit col-0">
								<a href="{{ $panel_data['relative_uri'].'/update/'.$row['id'] }}" class="btn btn-xs btn-default">
									<i class="fui-new"></i>
								</a>
							</td>
						@endif

						<? $c = 1; ?>
						@foreach($data['config']['elements'] as $elem_name => $elem_data)
							{{-- If the data is an array, we need to loop though THAT and get the names --}}
							
							@if(is_array($row[$elem_name]))
								<td class="col-{{ $c }}">
									@foreach($row[$elem_name] as $i => $sub_data)
										{{ $i > 0 ? '| ' : '' }}{{ $sub_data['name'] }} 
									@endforeach
								</td>
							@else
								<td class="col-{{ $c }}">
									{{ $row[$elem_name] }}
								</td>
							@endif

							<? $c++; ?>
						@endforeach

						@if ($user_can_delete)
							{{-- We need our button for delete --}}
							<td class="td-for-delete col-delete">
								<a href="{{ $panel_data['relative_uri'].'/update/'.$row['id'] }}" 
									class="btn btn-xs btn-danger list-record-deleter" 
									data-target="#deleter-modal" 
									data-toggle="modal">
									<i class="fui-cross"></i>
								</a>
							</td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>

		@if ($user_can_delete) {{-- our model for deletion confirmation is standing by --}}
			<div class="modal" id="deleter-modal" data-relative-uri="{{ $panel_data['relative_uri'] }}">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close fui-cross" data-dismiss="modal" aria-hidden="true"></button>
			        <h4 class="modal-title">Please Confirm. Are you sure?</h3>
			      </div>

			      <div class="modal-body">
			        <p>Do you definitely want to delete this {{ $single_item_name }}?</p>
			      </div>

			      <div class="modal-footer">
			        <a href="javascript:;" class="btn btn-default" data-dismiss="modal">Cancel</a>
			        <a href="javascript:;" class="btn btn-danger deletion-confirmer">Delete It</a>
			      </div>
			    </div>
			  </div>  
			</div>
		@endif
		
	@else {{-- we no have no stinking data --}}

		<div class="alert">
          <button type="button" class="close fui-cross" data-dismiss="alert"></button>

          <h4>You don't have any {{ str_plural(strtolower($single_item_name)) }} yet!</h4>

          @if ($user_can_create)
	      	<button type="button" class="btn btn-success btn-wide" onclick="window.location = '{{ $panel_data['relative_uri'].'/create' }}'">
	      		Add your first {{ strtolower($single_item_name) }} now!
	      		<span class="fui-arrow-right"></span>
	      	</button>
	      @else 
	      	<p>
	      		It doesn't look like you have access to create new 
	      		{{ str_plural(strtolower($single_item_name)) }} either, though.
	      	</p>
	      @endif
        </div>

	@endif

@stop