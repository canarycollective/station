@if(Session::has('success'))
    <div class="dialog dialog-success">
      {{ Session::get('success') }}
      <button class="btn btn-default" data-dismiss="alert">
        Close <span class="fui-cross"></span>
      </button>
    </div>
@endif

@if(Session::has('error'))
    <div class="dialog dialog-danger">
      {{ Session::get('error') }}
      <button class="btn btn-default" data-dismiss="alert">
        Close <span class="fui-cross"></span>
      </button>
    </div>
@endif

@if(Session::has('errors'))
    <div class="dialog dialog-danger">
      <p><b>There were some problems:</b></p>
      @foreach (Session::get('errors')->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      <button class="btn btn-default" data-dismiss="alert">
        Close <span class="fui-cross"></span>
      </button>
    </div>
@endif