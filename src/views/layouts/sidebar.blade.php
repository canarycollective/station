<ul class="nav nav-list nav-list-vivid" id="sidebar">
	@foreach($sidenav_data as $side_data)
		@if (isset($side_data['is_seperator']) && $side_data['is_seperator']) 
			<li class="nav-header">{{ $side_data['label'] }}</li>
		@else 
			<li class="{{ isset($curr_panel) && $curr_panel == $side_data['panel'] ? 'active' : '' }}">
				<a href="{{ $side_data['uri'] }}">
					{{ $side_data['label'] }}
				</a>
			</li>
		@endif
	@endforeach
</ul>