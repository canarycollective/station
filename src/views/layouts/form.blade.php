<?
	Form::macro('date', function($element_name)
	{
	    return '<div class="input-group"><span class="input-group-addon"><span class="fui-calendar"></span></span>'
	    		. Form::text($element_name,'',array('id'=>'station-'.$element_name, 'class'=>'form-control for-date', 'autocomplete' => 'off'))
	    		. '</div>';
	});
?>

@extends('station::layouts.base')

@section('content')
	
	{{-- show alert manually if desired --}}
	@if (isset($n_items_in_panel) and $n_items_in_panel == 0 and isset($panel_data['panel_options']['no_data_alert']))
		<div class="alert">
          <button type="button" class="close fui-cross" data-dismiss="alert"></button>
          <h4>{{ $panel_data['panel_options']['no_data_alert']['header'] }}</h4>
          <p>{{ $panel_data['panel_options']['no_data_alert']['body'] }}</p>
        </div>
	@endif

	{{-- header bar --}}
	<div class="clearfix">
		<div class="pull-left"><h5>{{ $layout_title }}</h5></div>
		@if (isset($n_items_in_panel) and $n_items_in_panel > 0)
			<div class="pull-right">
				<a href="{{ $panel_data['relative_uri'] }}" class="btn btn-lg btn-default list-new">
					<span class="fui-arrow-left"></span> Back To List
				</a>
			</div>
		@endif
	</div>

	@if($form_purpose=='create')

		{{ Form::open(array('class' => 'station-form', 'role'=>'form', 'url' => $form_action, 'method' => $form_method)) }}

	@else

		{{ Form::model($passed_model, array('class' => 'station-form', 'url' => $form_action, 'method' => $form_method)) }}

	@endif

		@foreach($panel_data['elements'] as $element_name => $element_info)

			<div class="form-group" {{ $element_info['type'] == 'hidden' ? 'style="display: none;"' : '' }}>
				
				<? 
					$id				= 'station-'.$element_name;
					$help			= isset($element_info['help']) && $element_info['help'] != '' ? $element_info['help'] : FALSE;
					$label			= $element_info['label'];
					$append_classes	= isset($element_info['format']) ? $element_info['format'] : '';
					$default_value 	= isset($element_info['default']) ? $element_info['default'] : null; // important to keep this as null
				?>

				{{-- show label if not a hidden field --}}
				@if ($element_info['type'] != 'hidden')
					<div class="label-wrap">
						{{ Form::label($id, $label) }}
						{{ $help ? '<span>'.$help.'</span>' : '' }}
					</div>
				@endif
				

				{{-- plain jane text entry, textarea, or hidden field --}}
				@if(in_array($element_info['type'],['text','email','date','time','datetime','textarea', 'hidden']))
					<? 
						$with_append		= isset($element_info['append']) && $element_info['append'] != '' ? 'with-append' : FALSE;
						$with_prepend_icon	= isset($element_info['prepend_icon']) && $element_info['prepend_icon'] != '' ? $element_info['prepend_icon'] : FALSE;
						$with_input_wrap	= $with_prepend_icon || $with_append;
						$attributes			= array('id' => $id, 'class'=>'form-control '.$append_classes, 'autocomplete' => 'off');
						
						if ($element_info['type'] == 'textarea' && isset($element_info['rows'])) $attributes['rows'] = $element_info['rows']; 
						if (isset($element_info['disabled']) && $element_info['disabled']) $attributes['disabled'] = 'disabled'; 
					?>
					{{ $with_input_wrap ? '<div class="input-group '.$with_append.'">' : '' }}

						@if ($with_prepend_icon)
							<span class="input-group-addon"><span class="{{ $with_prepend_icon }}"></span></span>
						@endif

						{{ Form::$element_info['type']($element_name, $default_value, $attributes) }}

						@if ($with_append)
							<span class="input-group-addon">{{ $element_info['append'] }}</span>
						@endif

					{{ $with_input_wrap ? '</div>' : '' }}
				@endif

				{{-- password field --}}
				@if($element_info['type']=='password')
					{{ Form::password($element_name,array('id' => $id,'class' => 'form-control')) }}
				@endif

				{{-- multiselect using foreign data --}}
				@if($element_info['type']=='multiselect' and isset($foreign_data[$element_name]))
					{{ Form::select($element_name.'[]', 
						$foreign_data[$element_name], 
						(Input::old($element_name) || !isset($passed_model)) ?: $passed_model->$element_name->lists('id'), 
						['multiple' => 'multiple','class'=>'chosen-select', 'style' => 'width: 400px', 'id' => $id, 'data-placeholder' => 'Please choose...']) }}
				@endif

				{{-- radio buttons --}}
				@if ($element_info['type'] == 'radio' and (isset($foreign_data[$element_name]) or (isset($element_info['data']['options']))))
					<? $options = isset($foreign_data[$element_name]) ? $foreign_data[$element_name] : $element_info['data']['options'] ?>
					<div class="radio-wrap">
						@foreach ($options as $item_id => $item_val)
							<label class="radio">{{ Form::radio($element_name, $item_id, null, ['id' => $id.'_'.$item_id]) }} {{ $item_val }}</label>
						@endforeach
					</div>
				@endif

				{{-- single select --}}
				@if ($element_info['type'] == 'select' and (isset($foreign_data[$element_name]) or (isset($element_info['data']['options']))))
					<? $options = isset($foreign_data[$element_name]) ? $foreign_data[$element_name] : $element_info['data']['options'] ?>
					{{ Form::select($element_name, $options, null,['class'=>'chosen-select', 'style' => 'width: 300px', 'id' => $id]) }}
				@endif

			</div>

		@endforeach

		<div class="pull-left">
			<button class="btn btn-success btn-hg station-form-submit">
				{{ $submit_value }} <i class="fui-arrow-right"></i>
			</button>
		
			<a href="{{ $panel_data['relative_uri'] }}" class="list-new form-canceler">
				or cancel
			</a>
		</div>

	{{ Form::close() }}

@stop